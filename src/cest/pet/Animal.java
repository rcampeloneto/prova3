package cest.pet;



public abstract class Animal {
	private String nome;
	private int idade;
	private TipoRaca raca;
		
	public Animal(String nome, int idade, TipoRaca raca, boolean doente) {
		super();
		this.nome = nome;
		this.idade = idade;
		this.raca = raca;
		this.doente = doente;
		TipoRaca t = new TipoRaca();
		t.setSigla("CA");
		t.setDescricao("cachorro");
		
		setTipo(t);
	}
	protected abstract void setTipo(TipoRaca t);
	public TipoRaca getRaca() {
		return raca;
	}
	public void setRaca(TipoRaca raca) {
		this.raca = raca;
	}
	private boolean doente;
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public int getIdade() {
		return idade;
	}
	public void setIdade(int idade) {
		this.idade = idade;
	}
	public boolean isDoente() {
		return doente;
	}
	public void setDoente(boolean doente) {
		this.doente = doente;
	}
	public String dormir() {
		return dormir();
	}
	public abstract void fazerBarulho();
	
	public abstract void procurarComida();
	
}
