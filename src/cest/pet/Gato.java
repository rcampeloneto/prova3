package cest.pet;

public class Gato extends Felino{
	public Gato(String nome, int idade, TipoRaca raca, boolean doente) {
		super(nome, idade, raca, doente);
		// TODO Auto-generated constructor stub
	}

	public void seEsfregarNasPernas() {
		System.out.println("Pedindo carinho");
	}

	@Override
	public void fazerBarulho() {
		System.out.println("Miar");
		
	}

	@Override
	public void procurarComida() {
		System.out.println("Ficar miando");
		
	}

}
